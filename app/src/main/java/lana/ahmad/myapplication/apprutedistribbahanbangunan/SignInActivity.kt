package lana.ahmad.myapplication.apprutedistribbahanbangunan

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.startActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_signin.*

class SignInActivity : AppCompatActivity(), View.OnClickListener {

    var fbAuth = FirebaseAuth.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signin)
        btnGps.setOnClickListener(this)
        btnRute.setOnClickListener(this)
        btnLogoff.setOnClickListener {
            fbAuth.signOut()
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }
    override fun onClick(p0: View?) {
        when(p0?.id){
            R.id.btnGps ->{
                val intent = Intent(this,GpsActivity::class.java)
                startActivity(intent)
            }
            R.id.btnRute ->{
                val intent = Intent(this,RuteActivity::class.java)
                startActivity(intent)
            }
        }
        }
}
